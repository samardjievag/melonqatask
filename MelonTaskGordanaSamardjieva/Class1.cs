﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using SeleniumExtras.WaitHelpers;
using System.Threading;

namespace MelonTaskGordanaSamardjieva
{
    [TestFixture]
    [Parallelizable]
    public class Test : TestBase
    {
        //Variables used in the contact forms
        string itemName = "Bugaboo cup holder";
        string itemUrl = "https://www.bugaboo.com/us-en/accessories/holders-and-trays/bugaboo-cup-holderplus-80500CH03.html";
        string descriptionText = "Some text...";
        string firstName = "Gordana";
        string lastName = "Samardjieva";
        string email = "samardjievag@gmail.com";
        string address1 = "Nikola";
        string city = "Krushevo";
        string countrySelectValue = "CountriesList.MK";
        string postalCode = "7550";

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //-------------------------------------------Test for Question 3 Contact Form: I am unable to place an order---------------------------------------------------------//

        [Test]
        [TestCaseSource(typeof(TestBase), "BrowserToRunWith")]
        public void MelonTask1Form1(String browserName)
        {
            SetUp(browserName);
            SetUpTask1();

            //Verify load of the Contact form Home Page
            IWebElement titleCustomerForm = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(".current-breadcrumb")));
            Assert.AreEqual("Consumer contact form", titleCustomerForm.Text);

            //Dropdown list for the Contact Forms
            SelectElement consumerQuestionForm = new SelectElement(Driver.FindElement(By.XPath("//*[@class='slds-select']")));
            consumerQuestionForm.SelectByValue("Question3");

            //Description field-required
            IWebElement descriptionFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@class='slds-textarea']")));
            descriptionFld.SendKeys(descriptionText);

            //Scroll down
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,750)");

            //First Name field-required
            IWebElement firstNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='First_Name']")));
            firstNameFld.SendKeys(firstName);

            //Last Name field-required
            IWebElement lastNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Last_Name']")));
            lastNameFld.SendKeys(lastName);

            //Email field-required
            IWebElement emailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Email']")));
            emailFld.SendKeys(email);

            //Verify Email field-required
            IWebElement vrfEmailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Verify_EMail']")));
            vrfEmailFld.SendKeys(email);

            //Street Address 1 field-required
            IWebElement address1Fld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Street_Address_1']")));
            address1Fld.SendKeys(address1);

            //City field-required
            IWebElement city1Fld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='City']")));
            city1Fld.SendKeys(city);

            //Dropdown list for choosing Country-required
            SelectElement countryFld = new SelectElement(Driver.FindElement(By.XPath("//*[@name='Country']")));
            countryFld.SelectByValue(countrySelectValue);

            //Postal COde field-required
            IWebElement postalCodeFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Postal_Code']")));
            postalCodeFld.SendKeys(postalCode);

            //----------------------------------------------------Tries for clicking reCAPTCHA tickbox-----------------------------------------------------------------------//
            //Wait(30);
            //new WebDriverWait(Driver, TimeSpan.FromSeconds(50)).Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.XPath("//iframe[@id='vfFrame']")));
            //Wait(30);
            //Driver.SwitchTo().Frame(Driver.FindElement(By.XPath("//*[@name='a-h327w1ncg25m']")));
            //var check = Driver.FindElement(By.ClassName("recaptcha-checkbox-checkmark"));
            //Wait(100);
            //check.Click();
            //Driver.SwitchTo().ParentFrame();
            //IWebElement recaptchaBox = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@class='recaptcha-checkbox-checkmark']")));
            //recaptchaBox.Click();

            //reCaptcha checkbox, it is positioned on the reChaptcha tickbox but it does NOT tick the box by using ENTER from the Keyboard
            vrfEmailFld.Click();
            vrfEmailFld.SendKeys(Keys.Tab + Keys.Tab + Keys.Tab + Keys.Tab + Keys.Tab + Keys.Tab + Keys.Enter + Keys.Enter);

            //Submit button click
            IWebElement selectBtn = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.ClassName("flow-button__NEXT")));
            selectBtn.Click();

            //Close the test
            TearDown();
        }
        //------------------------------The form for Question3 IS NOT SUBMITTED because the reCAPTCHA tickbox is not clicked.------------------------------------------------//




        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //----------------------------------Test for Question 6 Contact Form: I have a question about stock availability-----------------------------------------------------//

        [Test]
        [TestCaseSource(typeof(TestBase), "BrowserToRunWith")]
        public void MelonTask1Form2(String browserName)
        {
            SetUp(browserName);
            SetUpTask1();

            //Verify load of the Contact form Home Page
            IWebElement titleCustomerForm = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(".current-breadcrumb")));
            Assert.AreEqual("Consumer contact form", titleCustomerForm.Text);

            //Dropdown list for the Contact Forms
            SelectElement consumerQuestionForm = new SelectElement(Driver.FindElement(By.XPath("//*[@class='slds-select']")));
            consumerQuestionForm.SelectByValue("Question6");

            //Item Code/Name field-required
            IWebElement itemNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Item_Code']")));
            itemNameFld.SendKeys(itemName);

            //Scroll down
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,950)");

            //Link if on .com field
            IWebElement itemLinkFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@type='url']")));
            itemLinkFld.SendKeys(itemUrl);

            //Description field-required
            IWebElement descriptionFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@class='slds-textarea']")));
            descriptionFld.SendKeys(descriptionText);

            //First Name field-required
            IWebElement firstNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='First_Name']")));
            firstNameFld.SendKeys(firstName);

            //Last Name field-required
            IWebElement lastNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Last_Name']")));
            lastNameFld.SendKeys(lastName);

            //Email field-required
            IWebElement emailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Email']")));
            emailFld.SendKeys(email);

            //Verify Email field-required
            IWebElement vrfEmailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Verify_EMail']")));
            vrfEmailFld.SendKeys(email);

            //Street Address 1 field-required
            IWebElement address1Fld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Street_Address_1']")));
            address1Fld.SendKeys(address1);

            //City field-required
            IWebElement city1Fld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='City']")));
            city1Fld.SendKeys(city);

            //Scroll down
            js.ExecuteScript("window.scrollBy(0,450)");

            //Dropdown list for choosing Country-required
            SelectElement countryFld = new SelectElement(Driver.FindElement(By.XPath("//*[@name='Country']")));
            countryFld.SelectByValue(countrySelectValue);

            //reChaptcha checkbox
            Wait(30);
            new WebDriverWait(Driver, TimeSpan.FromSeconds(50)).Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.XPath("//iframe[@id='vfFrame']")));
            Wait(30);
            Driver.SwitchTo().ParentFrame();

            //Submit button click
            IWebElement selectBtn = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.ClassName("flow-button__NEXT")));
            selectBtn.Click();

            //Close the test
            TearDown();
        }
        //------------------------------The form for Question 6 IS NOT SUBMITTED because the reCAPTCHA tickbox is not clicked.-----------------------------------------------//



        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //-----------------------------------------Test for Question7 Contact Form: I have a question about a promotion------------------------------------------------------//

        [Test]
        [TestCaseSource(typeof(TestBase), "BrowserToRunWith")]
        public void MelonTask1Form3(String browserName)
        {

            SetUp(browserName);
            SetUpTask1();

            //Verify load of the Contact form Home Page
            IWebElement titleCustomerForm = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(".current-breadcrumb")));
            Assert.AreEqual("Consumer contact form", titleCustomerForm.Text);

            //Dropdown list for the Contact Forms
            SelectElement consumerQuestionForm = new SelectElement(Driver.FindElement(By.XPath("//*[@class='slds-select']")));
            consumerQuestionForm.SelectByValue("Question7");

            //Scroll down
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,950)");

            //Link if on .com field
            IWebElement itemLinkFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@type='url']")));
            itemLinkFld.SendKeys(itemUrl);

            //Description field-required
            IWebElement descriptionFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@class='slds-textarea']")));
            descriptionFld.SendKeys(descriptionText);

            //First Name field-required
            IWebElement firstNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='First_Name']")));
            firstNameFld.SendKeys(firstName);

            //Last Name field-required
            IWebElement lastNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Last_Name']")));
            lastNameFld.SendKeys(lastName);

            //Email field-required
            IWebElement emailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Email']")));
            emailFld.SendKeys(email);

            //Verify Email field-required
            IWebElement vrfEmailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Verify_EMail']")));
            vrfEmailFld.SendKeys(email);

            //Street Address 1 field-required
            IWebElement address1Fld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Street_Address_1']")));
            address1Fld.SendKeys(address1);

            //City field-required
            IWebElement city1Fld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='City']")));
            city1Fld.SendKeys(city);

            //Scroll down
            js.ExecuteScript("window.scrollBy(0,450)");

            //Dropdown list for choosing Country-required
            SelectElement countryFld = new SelectElement(Driver.FindElement(By.XPath("//*[@name='Country']")));
            countryFld.SelectByValue(countrySelectValue);

            //reChaptcha checkbox
            Wait(30);
            new WebDriverWait(Driver, TimeSpan.FromSeconds(50)).Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.XPath("//iframe[@id='vfFrame']")));
            Wait(30);
            Driver.SwitchTo().ParentFrame();

            //Submit button click
            IWebElement selectBtn = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.ClassName("flow-button__NEXT")));
            selectBtn.Click();

            //Close the test
            TearDown();
        }
        //------------------------------The form for Question 7 IS NOT SUBMITTED because the reCAPTCHA tickbox is not clicked.-----------------------------------------------//



        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //--------------------------------------------------------Test for Question8 Contact Form: Other question------------------------------------------------------------//

        [Test]
        [TestCaseSource(typeof(TestBase), "BrowserToRunWith")]
        public void MelonTask1Form4(String browserName)
        {
            SetUp(browserName);
            SetUpTask1();

            //Verify load of the Contact form Home Page
            IWebElement titleCustomerForm = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(".current-breadcrumb")));
            Assert.AreEqual("Consumer contact form", titleCustomerForm.Text);

            //Dropdown list for the Contact Forms
            SelectElement consumerQuestionForm = new SelectElement(Driver.FindElement(By.XPath("//*[@class='slds-select']")));
            consumerQuestionForm.SelectByValue("Question8");

            //Description field-required
            IWebElement descriptionFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@class='slds-textarea']")));
            descriptionFld.SendKeys(descriptionText);

            //Scroll down
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,750)");

            //First Name field-required
            IWebElement firstNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='First_Name']")));
            firstNameFld.SendKeys(firstName);

            //Last Name field-required
            IWebElement lastNameFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Last_Name']")));
            lastNameFld.SendKeys(lastName);

            //Email field-required
            IWebElement emailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Email']")));
            emailFld.SendKeys(email);

            //Verify Email field-required
            IWebElement vrfEmailFld = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@name='Verify_EMail']")));
            vrfEmailFld.SendKeys(email);

            //Dropdown list for choosing Country-required
            SelectElement countryFld = new SelectElement(Driver.FindElement(By.XPath("//*[@name='Country']")));
            countryFld.SelectByValue(countrySelectValue);

            //reChaptcha checkbox
            Wait(30);
            new WebDriverWait(Driver, TimeSpan.FromSeconds(50)).Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.XPath("//iframe[@id='vfFrame']")));
            Wait(30);
            Driver.SwitchTo().ParentFrame();

            //Submit button click
            IWebElement selectBtn = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.ClassName("flow-button__NEXT")));
            selectBtn.Click();

            //Close the test
            TearDown();
        }
        //------------------------------The form for Question 8 IS NOT SUBMITTED because the reCAPTCHA tickbox is not clicked.-----------------------------------------------//




        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //-------------------------------------------------------------------Task 2 - BROCKER LIST---------------------------------------------------------------------------//

        [Test]
        [TestCaseSource(typeof(TestBase), "BrowserToRunWith")]
        public void MelonTask2(String browserName)
        {
            SetUp(browserName);
            SetUpTask2();

            //Verify load of Brocker Page
            IWebElement titleBrokerPage = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(".brokers-section-title")));
            Assert.AreEqual("БРОКЕРИ", titleBrokerPage.Text);

            //Scroll down
            Wait(10);
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0,750)");

            //Load more Brocker button
            Thread.Sleep(3000);
            IWebElement ЗаредиOщеBtn = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector("a[data-container='load-more-brokers']")));
            ЗаредиOщеBtn.Click();
            Thread.Sleep(3000);
            Wait(20);

            //Scroll up
            js.ExecuteScript("window.scrollTo(0,0)");
            IWebElement searchBox = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector("input[data-container='broker-keyword']")));

            //Make list of all Brockers and search for every brocker NAME
            List<IWebElement> allBrockerNames = Driver.FindElements(By.CssSelector(".broker-list h3.name a")).ToList();
            List<String> allNames = new List<string>();

            foreach (var name in allBrockerNames)
            {
                allNames.Add(name.Text);
            }

            foreach (var name in allNames)
            {

                searchBox.SendKeys(name);

                
                Thread.Sleep(1000);
                IWebElement searchResultCard = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(".broker-list-holder .broker-card .name a")));
                Thread.Sleep(1000);

                //Assert for one searched Brocker displayed
                Assert.AreEqual(name, searchResultCard.Text);
                Thread.Sleep(1000);

                //Assert for Address displayed
                Assert.IsTrue(Driver.FindElement(By.ClassName("office")).Displayed);
                Thread.Sleep(500);

                //Assert for Properties displayed
                Assert.IsTrue(Driver.FindElement(By.PartialLinkText("имота")).Displayed);
                Thread.Sleep(500);

                 //Assert for Phone Numbers

                 //--------------At Антония Спасова Phone Numbers the Assert Fails because she has only one phone number displayed on the search result screen---------------//
                Assert.IsTrue(Driver.FindElement(By.XPath("//span[@class='tel']")).Displayed);
                Assert.IsTrue(Driver.FindElement(By.XPath("//span[@class='tel']/following-sibling::span")).Displayed);
                Thread.Sleep(1000);

                //Clear the search box so other name can be searched
                searchBox.Clear();
            }

            //Close the test
            TearDown();
        }
         

    }
}
