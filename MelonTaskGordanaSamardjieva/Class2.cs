﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MelonTaskGordanaSamardjieva
{
    public class TestBase
    {
        protected IWebDriver Driver;
        public WebDriverWait DriverWait;

        //SetUp for Browser Driver
        public void SetUp(String browserName)
        {
            if (browserName.Equals("chrome"))
                Driver = new ChromeDriver();
            else
                Driver = new FirefoxDriver();

            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
        }

        public static IEnumerable<String> BrowserToRunWith()
        {
            String[] browsers = { "chrome", "fireFox" };

            foreach (String b in browsers)
                yield return b;
        }



        public void SetUpTask1()
        {
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Navigate().GoToUrl("https://service.bugaboo.com/s/consumer-contact?selectedItem=Consumer_Contact_Form__c&language=en_US");
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);

            IWebElement acceptCookies = DriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.CssSelector("button[id='CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll']")));
            acceptCookies.Click();
        }


        public void SetUpTask2()
        {
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Navigate().GoToUrl("https://www.yavlena.com/broker/");
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);
        }


        public void Wait(int seconds)
        {
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(seconds));
        }

        public void TearDown()
        {
            Driver.Quit();
        }

    }
}
